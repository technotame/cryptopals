#include <stdio.h>

// Hex: 49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d
// Base64: SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t

char* convert (char *);

int main(int argc, char *argv[])
{


char hex[] = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
int hex_size = sizeof(hex) / sizeof(hex[0]);
char base64set[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	printf("The size of hex is %d\n", hex_size);
	for ( int i = 0; i < hex_size +2; i++)
	{
		printf("%c", hex[i]);
	}
	if(hex_size % 3 != 0) {
		printf("\nNot divisible by 3! Remainder is %d. Needs padding.\n", hex_size % 3);
	};
	
char converted[5];
for( int j = 0; j < hex_size; j+=3)
{

	//int test[4] = {0x49, 0x27, 0x6d};
	int test[4] = {0x6f, 0x6f, 0x6d};
	int byte1, byte2, byte3, byte4 = 0;
	
	// mask byte 1, shift right 2, convert
	int b1mask1 = 252;
	int b1masked1 = test[0] & b1mask1;
	int b1shifted1 = b1masked1 >> 2;
	converted[0] = base64set[b1shifted1];
	
	// mask byte 1 last 2, shift left 4
	int b1mask2 = 3;
	int b1masked2 = test[0] & b1mask2;
	int b1shifted2 = b1masked2 << 4;
	// mask byte 2 first 4, shift right 4
	int b2mask1 = 240;
	int b2masked1 = test[1] & b2mask1;
	int b2shifted1 = b2masked1 >> 4;
	// add masked/shifted byte 1 and masked/shifted byte2
	int addb1b2 = b1shifted2 + b2shifted1;
	converted[1] = base64set[addb1b2];
	
	// mask byte 2 last 4, shift left 2
	int b2mask2 = 15;
	int b2masked2 = test[1] & b2mask2;
	int b2shifted2 = b2masked2 << 2;
	// mask byte 3 first 2, shift right 6
	int b3mask1 = 192;
	int b3masked1 = test[2] & b3mask1;
	int b3shifted1 = b3masked1 >> 6;
	// add masked/shifted byte 2 last and masked/shifted byte 3 first 2
	int addb2b3 = b2shifted2 + b3shifted1;
	converted[2] = base64set[addb2b3];
	
	// mask byte 3 last 6
	int b3mask2 = 63;
	int b3masked2 = test[2] & b3mask2;
	converted[3] = base64set[b3masked2];
	
};	
	
puts(converted);
	
return 0;
}
